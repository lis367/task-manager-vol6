package com.company;

import com.company.exception.CommandCorruptException;
import com.company.util.Bootstrap;

public class Application {

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.start();
    }

}
