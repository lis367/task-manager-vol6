package com.company.exception;

public class CommandCorruptException extends Exception {
    @Override
    public String toString() {
        return "CommandCorruptException";
    }
}
