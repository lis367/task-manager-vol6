package com.company.service;

import com.company.entity.Task;
import com.company.exception.ObjectIsNotFound;
import com.company.repository.TaskRepository;
import com.company.util.Bootstrap;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class TaskService {

    private TaskRepository taskRepository;
    private ProjectService projectService;
    private Bootstrap bootstrap;



    public String taskCreate(String name, String projectId, Date dateStart, Date dateEnd, String userId) throws ObjectIsNotFound {
        if (projectService.read(projectId)==null){
           throw new ObjectIsNotFound();
        }
        if(projectService.read(projectId).getUserId()!=bootstrap.getUser().getUserId()){
            System.out.println("You can't create task with projects of others users");
            throw new ObjectIsNotFound();
        }
        String id = UUID.randomUUID().toString();
        Task task = new Task(name, id, userId);
        task.setDateBegin(dateStart);
        task.setDateEnd(dateEnd);
        task.setProjectID(projectId);
        taskRepository.merge(task);
        return task.getId();
    }

    public ArrayList taskList() {
        return taskRepository.findAll(bootstrap.getUser().getUserId());
    }

    public void taskClear() {
        taskRepository.removeAll();
    }

    public void taskRemove(String id) throws ObjectIsNotFound {
        if(taskRepository.findOne(id)==null){
            throw new ObjectIsNotFound();
        }
        else {
            taskRepository.remove(id);
        }
    }
    public Task findTask (String id){
        return taskRepository.findOne(id);
    }

    public void updateService(Task task){
        taskRepository.update(task);
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
