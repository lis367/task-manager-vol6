package com.company.service;

import com.company.entity.Project;
import com.company.exception.ObjectIsNotFound;
import com.company.repository.ProjectRepository;
import com.company.repository.TaskRepository;
import com.company.util.Bootstrap;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;
    private Bootstrap bootstrap;

    public String projectCreate(String name, Date dateStart, Date dateEnd, String userId) throws Exception {
        String id = UUID.randomUUID().toString();
        Project project = new Project(name, id, userId);
        project.setDateBegin(dateStart);
        project.setDateEnd(dateEnd);
        projectRepository.persist(project);
        return project.getId();
    }

    public ArrayList projectList() {
        return projectRepository.findAll(bootstrap.getUser().getUserId());
    }

    public void projectClear() {
        projectRepository.removeAll();
    }

    public void projectRemove(String id) throws ObjectIsNotFound {
        if(projectRepository.findOne(id)==null){
            throw new ObjectIsNotFound();
         }
        else {
            projectRepository.remove(id);
            ArrayList<String>copyofTask = taskRepository.getTasksFromProject(id);
            for(int i=0;i<copyofTask.size();i++){
                taskRepository.remove(copyofTask.get(i));
            }
        }
    }

    public Project read(String id) throws ObjectIsNotFound {
        return projectRepository.findOne(id);
    }


    public void update(Project project){
        projectRepository.merge(project);
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
