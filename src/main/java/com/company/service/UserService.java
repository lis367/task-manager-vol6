package com.company.service;
import com.company.entity.User;
import com.company.entity.UserRoleType;
import com.company.exception.ObjectIsNotFound;
import com.company.repository.UserRepository;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.UUID;

public class UserService {

    private UserRepository userRepository;

public void registration() throws Exception {
    System.out.println("ENTER LOGIN");
    Scanner sc = new Scanner(System.in);
    String login = sc.nextLine();
    System.out.println("ENTER PASSWORD");
    String password = generateMD5(sc.nextLine());
    User user = new User(login,password);
    String id =  UUID.randomUUID().toString();
    user.setUserId(id);
    user.setUserRoleType(UserRoleType.USER);
    userRepository.persist(user);
    System.out.println("User id = "+ id + " with login "+login +" password "+password);
}

public void update(User user) throws ObjectIsNotFound, IOException {
  userRepository.update(user);
  userRepository.save();
}


public String generateMD5 (String password) throws NoSuchAlgorithmException {
    MessageDigest m= MessageDigest.getInstance("MD5");
    m.update(password.getBytes());
    byte[] digest = m.digest();
    StringBuilder builder = new StringBuilder();
    for (byte b : digest) {
        builder.append(String.format("%02x", b & 0xff));
    }
    return builder.toString();
}

public User getUserFromRepository (String login, String password){
    return userRepository.getUser(login,password);
}

public void save (User user) throws Exception {
    userRepository.persist(user);
}
public void load() throws IOException {
    userRepository.loadFromFile();
}

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}
