package com.company.entity;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Project {
    String name;
    String id;
    String description;
    Date dateBegin;
    Date dateEnd;
    String userId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date begins) {
        this.dateBegin = begins;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date ends) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        this.dateEnd = ends;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project(String name, String id, String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
