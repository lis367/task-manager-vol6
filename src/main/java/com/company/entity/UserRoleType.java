package com.company.entity;

public enum UserRoleType {

    ADMIN("Admin"),
    USER("User");

    String displayName;

    UserRoleType(String displayName) {
        this.displayName = displayName;
    }

    /*
    @Override
    public String toString() {
        return "displayName = " + displayName;
    }

     */
}
