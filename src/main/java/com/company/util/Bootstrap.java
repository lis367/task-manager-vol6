package com.company.util;

import com.company.commands.AbstractCommand;
import com.company.commands.HelpCommand;
import com.company.commands.projectCommands.*;
import com.company.commands.taskCommands.*;
import com.company.commands.userCommands.*;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.entity.User;
import com.company.exception.CommandCorruptException;
import com.company.repository.ProjectRepository;
import com.company.repository.TaskRepository;
import com.company.repository.UserRepository;
import com.company.service.ProjectService;
import com.company.service.TaskService;
import com.company.service.UserService;

import java.util.*;

public final class Bootstrap {

    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    private User user;

    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    public void start() throws Exception {
        execute("user-load");
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
            System.out.println();
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            return;}
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            return; }
        if ((user==null)&&(abstractCommand.secureCommand())){
            System.out.println("SECURE COMMAND. YOU NEED TO LOG IN");
        }
        else{
        abstractCommand.execute();}
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init() throws CommandCorruptException {

        // Созданы Проджект и Таск сервисы/репозитории

        ProjectService projectService = new ProjectService();
        TaskService taskService = new TaskService();
        Map<String, Project> projectRepositoryMap = new HashMap<>();
        Map<String, Task> taskRepositoryMap = new HashMap<>();
        TaskRepository taskRepository = new TaskRepository(taskRepositoryMap);
        ProjectRepository projectRepository = new ProjectRepository(projectRepositoryMap,taskRepository);

        projectService.setProjectRepository(projectRepository);
        projectService.setTaskRepository(taskRepository);
        projectService.setBootstrap(this);
        taskService.setTaskRepository(taskRepository);
        taskService.setProjectService(projectService);
        taskService.setBootstrap(this);

        Map<String,User> userRepositoryMap = new HashMap<>();
        UserRepository userRepository = new UserRepository(userRepositoryMap);
        UserService userService = new UserService();
        userService.setUserRepository(userRepository);

        // Созданы комманды и загружены в лист Commands через registry

        AbstractCommand[] commandsInit = {new HelpCommand(this), new ProjectClearCommand(projectService,taskService, this),
                new ProjectCreateCommand(projectService, this), new ProjectListCommand(projectService, this),
                new ProjectRemoveCommand(projectService, this), new TaskClearCommand(taskService,this),new TaskCreateCommand(taskService, this),
                new TaskListCommand(taskService,this), new TaskRemoveCommand(taskService, this),
                new UserAuthorizationCommand(userService, this),
                new UserEndSessionCommand(userService,this), new UserRegistrationCommand(userService),
                new UserUpdateCommand(userService,this),
                new UserLoadFromDBCommand(userService), new UserUpdatePasswordCommand(userService, this),
                new UserViewCommand(this),new ProjectUpdateCommand(projectService, this),
                new ProjectViewCommand(projectService, this), new TaskUpdateCommand(taskService, this),
                new TaskViewCommand(taskService, this),
                
        };

        commands.clear();
        for (AbstractCommand commands: commandsInit){
            registry(commands);
        }

    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
