package com.company.repository;


import com.company.entity.User;
import com.company.entity.UserRoleType;
import com.company.exception.ObjectIsAlreadyExist;
import com.company.exception.ObjectIsNotFound;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class UserRepository {
    public Map<String, User> userRepository;


    public void loadFromFile () throws IOException {
        FileReader fr = new FileReader("D:\\gitlab\\DB.txt");
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null){
            String[] arg = line.split(":");
            String login = arg[0];
            String password = arg[1];
            String userType = arg[2];
            String userId = arg[3];
            User user = new User(login,password);
            user.setUserId(userId);
            UserRoleType userRoleType;
            if(userType.equalsIgnoreCase("ADMIN")){
                userRoleType = UserRoleType.ADMIN;
            }
            else {
                userRoleType = UserRoleType.USER;
            }
            user.setUserRoleType(userRoleType);
            userRepository.put(userId,user);
        }
        br.close();
        fr.close();
    }

    public User getUser(String login, String password){
        for (Map.Entry <String, User> userEntry: userRepository.entrySet()){
            if(userEntry.getValue().getName().equals(login)&&userEntry.getValue().getPassword().equals(password)){
                return userEntry.getValue();
            }
        }
            return null;
    }

    public void persist(User user) throws Exception
    {
        User userPersist = userRepository.get(user.getUserId());
        if (userPersist ==null){
            userRepository.put(user.getUserId(),user);
            save();
        }
        else throw new ObjectIsAlreadyExist();
    }

    public void update(User user) throws ObjectIsNotFound {
        User userUpdate = userRepository.get(user.getUserId());
        if(userUpdate ==null){
            throw new ObjectIsNotFound();
        }
        else {
            userRepository.put(user.getUserId(), user);
        }
    }

    public void save() throws IOException {
        FileWriter fw = new FileWriter("D:\\gitlab\\DB.txt");
        for (Map.Entry <String, User> userEntry: userRepository.entrySet()){
            fw.write(userEntry.getValue().getName()+":"+userEntry.getValue().getPassword()+":"+userEntry.getValue().getUserRoleType()+":"+userEntry.getValue().getUserId()+"\n");
            }
        fw.close();
        }

    public UserRepository(Map<String, User> userRepository) {
        this.userRepository = userRepository;
    }


}
