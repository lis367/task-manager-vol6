package com.company.repository;

import com.company.entity.Project;
import com.company.entity.Task;
import com.company.exception.ObjectIsAlreadyExist;

import java.util.ArrayList;
import java.util.Map;

public class TaskRepository {


    private Map<String, Task> taskRepository;

    public void persist(Task task) throws Exception {
        Task taskPersist = taskRepository.get(task.getId());
        if (taskPersist ==null){
            taskRepository.put(task.getId(), task);
        }
        else throw new ObjectIsAlreadyExist();
    }

    public Task findOne(String id){
        return taskRepository.get(id);
    }

    public ArrayList findAll(String userID){
        ArrayList<Task> tasks = new ArrayList<>();
        for (Map.Entry<String, Task> taskPool: taskRepository.entrySet()){
            if((taskPool.getValue().getUserId())==(userID)){
                tasks.add(taskPool.getValue());
            }
        }
        return tasks;
    }

    public void merge (Task task)
    {
        taskRepository.put(task.getId(), task);
    }

    public void remove(String id){
        taskRepository.remove(id);
    }

    public void removeAll(){
        taskRepository.clear();
    }

    public void update(Task task){
       taskRepository.put(task.getId(),task);
    }

    public ArrayList getTasksFromProject(String projectID){
        ArrayList<Task> arrayList = new ArrayList<Task>();
        for (Map.Entry<String, Task> taskEntry : taskRepository.entrySet()) {
           if(projectID.equals(taskEntry.getValue().getProjectID())){
               arrayList.add(taskEntry.getValue());
            }
        }
        return arrayList;
    }

    public void setTaskRepositoryMap(Map<String, Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    public TaskRepository(Map<String, Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

}

