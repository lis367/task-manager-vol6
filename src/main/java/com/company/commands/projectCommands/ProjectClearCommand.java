package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.UserRoleType;
import com.company.service.ProjectService;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

public final class ProjectClearCommand extends AbstractCommand {

    private ProjectService projectService;
    private TaskService taskService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        projectService.projectClear();
        taskService.taskClear();
        System.out.println("ALL PROJECT&TASK REMOVED");}
        else {
            System.out.println("ADMIN ROLETYPE ARE REQUIRED");
        }
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }


    public ProjectClearCommand(ProjectService projectService, TaskService taskService, Bootstrap bootstrap) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }
    public boolean secureCommand() {
        return true;
    }




}
