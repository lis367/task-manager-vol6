package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.ProjectService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public final class ProjectRemoveCommand extends AbstractCommand {

    private ProjectService projectService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if(projectService.read(line).getUserId().equals(bootstrap.getUser().getUserId())){
            projectService.projectRemove(line);
                System.out.println("PROJECT&TASK REMOVED");

            }
            else{
                System.out.println("no rights to delete project of other users");
            }
        } catch (ObjectIsNotFound objectIsNotFound) {
            System.out.println("PROJECT IS NOT FOUND");
        }


    }
    public boolean secureCommand() {
        return true;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public ProjectRemoveCommand(ProjectService projectService, Bootstrap bootstrap) {
        this.projectService = projectService;
        this.bootstrap = bootstrap;

    }

}
