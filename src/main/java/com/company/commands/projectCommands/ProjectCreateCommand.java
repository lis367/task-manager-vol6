package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.service.ProjectService;
import com.company.util.Bootstrap;

import java.text.SimpleDateFormat;
import java.util.Scanner;

public final class ProjectCreateCommand extends AbstractCommand {

    private ProjectService projectService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("ENTER NAME:");
        String name = sc.nextLine().trim();
        if (name.isEmpty()) {
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
        String dateStart = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE PROJECT");
        String dateEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            System.out.println("YOUR PROJECT ID IS");
            System.out.println(projectService.projectCreate(name, dateFormatter.parse(dateStart),dateFormatter.parse(dateEnd), bootstrap.getUser().getUserId()));
        } catch (Exception e) {
            System.out.println("Wrong format dd.mm.yyyy");

        }

    }
    public boolean secureCommand() {
        return true;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }
    public ProjectCreateCommand(ProjectService projectService, Bootstrap bootstrap) {
        this.projectService = projectService;
        this.bootstrap = bootstrap;
    }


}
