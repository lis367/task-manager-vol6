package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.service.ProjectService;
import com.company.util.Bootstrap;

import java.text.SimpleDateFormat;
import java.util.Scanner;

public class ProjectUpdateCommand extends AbstractCommand {

    private ProjectService projectService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if (projectService.read(line).getUserId().equals(bootstrap.getUser().getUserId())) {
                Project projectPool = projectService.read(line);
                System.out.println("ENTER NEW NAME");
                line = sc.nextLine();
                projectPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE PROJECT");
                String dateStart = sc.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE PROJECT");
                String dateEnd = sc.nextLine().trim();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                projectPool.setDateBegin(dateFormatter.parse(dateStart));
                projectPool.setDateEnd(dateFormatter.parse(dateEnd));
                projectService.update(projectPool);
                System.out.println("SUCCESS");
            } else {
                System.out.println("no rights to update projects of other users");
            }
        }
        catch (NullPointerException e){
            System.out.println("Project is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectUpdateCommand(ProjectService projectService, Bootstrap bootstrap) {
        this.projectService = projectService;
        this.bootstrap = bootstrap;
    }
}
