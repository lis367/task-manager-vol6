package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.service.ProjectService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public class ProjectViewCommand extends AbstractCommand {
    private ProjectService projectService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try{
            if (projectService.read(line).getUserId().equals(bootstrap.getUser().getUserId())){
                System.out.println("Project name "+projectService.read(line).getName());
                System.out.println("Project begins in "+projectService.read(line).getDateBegin());
                System.out.println("Project ends in "+ projectService.read(line).getDateEnd());
            }
            else {
                System.out.println("no rights to update projects of other users");
            }
        }
        catch (NullPointerException npe){
            System.out.println("Project is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public ProjectViewCommand(ProjectService projectService, Bootstrap bootstrap) {
        this.projectService = projectService;
        this.bootstrap = bootstrap;
    }
}
