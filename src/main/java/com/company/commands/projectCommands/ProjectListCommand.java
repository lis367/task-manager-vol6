package com.company.commands.projectCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.service.ProjectService;
import com.company.util.Bootstrap;

import java.util.ArrayList;

public final class ProjectListCommand extends AbstractCommand {

    private ProjectService projectService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "project-list";
    }
    @Override
    public String description() {
        return "Show all projects.";
    }
    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        ArrayList <Project> projectPool = projectService.projectList();
        for(int i=0; i<projectPool.size();i++){
            System.out.println("Project Name: "+projectPool.get(i).getName() + " Project ID: " + projectPool.get(i).getId());
         }

    }
    public boolean secureCommand() {
        return true;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public ProjectListCommand(ProjectService projectService, Bootstrap bootstrap) {
        this.projectService = projectService;
        this.bootstrap = bootstrap;
    }
}
