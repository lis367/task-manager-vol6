package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public final class TaskCreateCommand extends AbstractCommand {

    private TaskService taskService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine().trim();
        if(line.isEmpty()){
            System.out.println("EMPTY NAME");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String projectID = sc.nextLine().trim();
        System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
        String dateStart = sc.nextLine().trim();
        System.out.println("ENTER THE END TIME OF THE TASK");
        String dateEnd = sc.nextLine().trim();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
        try{
            System.out.println("TASK ID IS");
            System.out.println(taskService.taskCreate(line , projectID, dateFormatter.parse(dateStart) , dateFormatter.parse(dateEnd), bootstrap.getUser().getUserId()) );
        }
        catch (ParseException e){
            System.out.println("Wrong format dd.mm.yyyy");
        }
        catch (ObjectIsNotFound e){
            System.out.println("Такого проекта не сущесвует");
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskCreateCommand(TaskService taskService, Bootstrap bootstrap) {
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }
}
