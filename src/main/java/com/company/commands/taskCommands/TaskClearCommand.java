package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.UserRoleType;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

public final class TaskClearCommand extends AbstractCommand {

    private TaskService taskService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUser().getUserRoleType().equals(UserRoleType.ADMIN)){
        taskService.taskClear();
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskClearCommand(TaskService taskService, Bootstrap bootstrap) {
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }

}
