package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public class TaskViewCommand extends AbstractCommand {
    private TaskService taskService;
    private Bootstrap bootstrap;


    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "Date and name view of project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try{
            if (taskService.findTask(line).getUserId().equals(bootstrap.getUser().getUserId())){
                System.out.println("Task name "+taskService.findTask(line).getName());
                System.out.println("Task begins in "+taskService.findTask(line).getDateBegin());
                System.out.println("Task ends in "+ taskService.findTask(line).getDateEnd());
            }
            else {
                System.out.println("no rights to update task of other users");
            }
        }
        catch (NullPointerException npe){
            System.out.println("Task is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskViewCommand(TaskService taskService, Bootstrap bootstrap) {
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }
}
