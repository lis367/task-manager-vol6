package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.Task;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

import java.util.ArrayList;

public final class TaskListCommand extends AbstractCommand {

    private TaskService taskService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        ArrayList<Task> tasksPool = taskService.taskList();
        for(int i=0;i<tasksPool.size();i++){
            System.out.println("Task Name: "+tasksPool.get(i).getName() + " Task ID: " + tasksPool.get(i).getId() + " Project :"+tasksPool.get(i).getProjectID());
        }
    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskListCommand(TaskService taskService,Bootstrap bootstrap) {
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }

}
