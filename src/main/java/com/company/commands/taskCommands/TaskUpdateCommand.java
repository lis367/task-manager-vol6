package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.Project;
import com.company.entity.Task;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

import java.text.SimpleDateFormat;
import java.util.Scanner;

public class TaskUpdateCommand extends AbstractCommand {
    private TaskService taskService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if (taskService.findTask(line).getUserId().equals(bootstrap.getUser().getUserId())) {
                Task taskPool = taskService.findTask(line);
                System.out.println("ENTER NEW NAME");
                line = sc.nextLine();
                taskPool.setName(line);
                System.out.println("ENTER THE TIME OF THE BEGINNING OF THE TASK");
                String dateStart = sc.nextLine().trim();
                System.out.println("ENTER THE END TIME OF THE TASK");
                String dateEnd = sc.nextLine().trim();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                taskPool.setDateBegin(dateFormatter.parse(dateStart));
                taskPool.setDateEnd(dateFormatter.parse(dateEnd));
                taskService.updateService(taskPool);
                System.out.println("SUCCESS");
            } else {
                System.out.println("no rights to update tasks of other users");
            }
        }
        catch (NullPointerException e){
            System.out.println("Task is not found");
        }
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public TaskUpdateCommand(TaskService taskService, Bootstrap bootstrap) {
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }
}
