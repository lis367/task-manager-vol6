package com.company.commands.taskCommands;

import com.company.commands.AbstractCommand;
import com.company.exception.ObjectIsNotFound;
import com.company.service.TaskService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public final class TaskRemoveCommand extends AbstractCommand {

    private TaskService taskService;
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER ID");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        try {
            if(taskService.findTask(line).getUserId().equals(bootstrap.getUser().getUserId())){
            taskService.taskRemove(line);
            System.out.println("TASK REMOVED");}
            else {
                System.out.println("no rights to delete task of other users");
            }
        }
        catch (ObjectIsNotFound objectIsNotFound) {
            objectIsNotFound.printStackTrace();

        }


    }
    public boolean secureCommand() {
        return true;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public TaskRemoveCommand(TaskService taskService, Bootstrap bootstrap) {
        this.taskService = taskService;
        this.bootstrap = bootstrap;
    }


}
