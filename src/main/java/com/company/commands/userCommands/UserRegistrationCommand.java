package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.entity.UserRoleType;
import com.company.service.UserService;

import java.util.Scanner;
import java.util.UUID;

public class UserRegistrationCommand extends AbstractCommand {

    private UserService userService;

    @Override
    public String command() {
        return "user-registration";
    }

    @Override
    public String description() {
        return "Registration";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER PASSWORD");
        String password = userService.generateMD5(sc.nextLine());
        User user = new User(login,password);
        String id =  UUID.randomUUID().toString();
        user.setUserId(id);
        user.setUserRoleType(UserRoleType.USER);
        userService.save(user);
        System.out.println("User id = "+ id + " with login "+login +" password "+password);
    }

    public boolean secureCommand() {
        return false;
    }


    public UserRegistrationCommand(UserService userService) {
        this.userService = userService;
    }
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
