package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.service.UserService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public class UserUpdateCommand extends AbstractCommand {


    private UserService userService;
    private Bootstrap bootstrap;


    @Override
    public String command() {
        return "user-update";
    }

    @Override
    public String description() {
        return "Update User";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER NEW LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER NEW PASSWORD");
        String password = userService.generateMD5(sc.nextLine());
        User user = bootstrap.getUser();
        user.setName(login);
        user.setPassword(password);
        bootstrap.setUser(user);
        userService.update(user);

    }

    public boolean secureCommand() {
        return true;
    }

    public UserUpdateCommand(UserService userService, Bootstrap bootstrap) {
        this.userService = userService;
        this.bootstrap = bootstrap;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
