package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.entity.User;
import com.company.service.UserService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public class UserUpdatePasswordCommand extends AbstractCommand {

    private UserService userService;
    private Bootstrap bootstrap;


    @Override
    public String command() {
        return "user-newPassword";
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER NEW PASSWORD");
        Scanner sc = new Scanner(System.in);
        String password = userService.generateMD5(sc.nextLine());
        User user = bootstrap.getUser();
        user.setPassword(password);
        bootstrap.setUser(user);
        userService.update(bootstrap.getUser());
        System.out.println("SUCCESS");
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserUpdatePasswordCommand(UserService userService, Bootstrap bootstrap) {
        this.userService = userService;
        this.bootstrap = bootstrap;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
