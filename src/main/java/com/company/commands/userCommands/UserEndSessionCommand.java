package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.service.UserService;
import com.company.util.Bootstrap;

public class UserEndSessionCommand extends AbstractCommand {
    private UserService userService;
    private Bootstrap bootstrap;


    @Override
    public String command() {
        return "user-end";
    }

    @Override
    public String description() {
        return "End of User session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOG OUT SUCCESSFUL");
        bootstrap.setUser(null);
    }
    public boolean secureCommand() {
        return true;
    }

    public UserEndSessionCommand(UserService userService, Bootstrap bootstrap) {
        this.userService = userService;
        this.bootstrap = bootstrap;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
