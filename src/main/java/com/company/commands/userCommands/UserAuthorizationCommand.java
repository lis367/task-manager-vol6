package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.service.UserService;
import com.company.util.Bootstrap;

import java.util.Scanner;

public class UserAuthorizationCommand extends AbstractCommand  {

    private UserService userService;
    private Bootstrap bootstrap;


    public String command() {
        return "user-authorization";
    }


    public String description() {
        return "Authorization";
    }


    public void execute() throws Exception {
        System.out.println("ENTER LOGIN");
        Scanner sc = new Scanner(System.in);
        String login = sc.nextLine();
        System.out.println("ENTER PASSWORD");
        String password = userService.generateMD5(sc.nextLine());
        if((userService.getUserFromRepository(login,password))==null){
            System.out.println("USER NOT FOUND");
            System.out.println("Registration:");
            userService.registration();
        }
        else {
            System.out.println("Authorization success");
            bootstrap.setUser(userService.getUserFromRepository(login,password));
        }

    }
    public boolean secureCommand() {
        return false;
    }


    public UserAuthorizationCommand(UserService userService, Bootstrap bootstrap) {
        this.userService = userService;
        this.bootstrap = bootstrap;
    }


    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
