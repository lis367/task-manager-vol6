package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.service.UserService;
import com.company.util.Bootstrap;

public class UserViewCommand extends AbstractCommand {
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "user-view";
    }

    @Override
    public String description() {
        return "User view";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("User name: "+bootstrap.getUser().getName());
        System.out.println("User id: "+bootstrap.getUser().getUserId());
        System.out.println("User roletype: "+bootstrap.getUser().getUserRoleType());
        System.out.println("User password: "+bootstrap.getUser().getPassword());
    }

    @Override
    public boolean secureCommand() {
        return true;
    }

    public UserViewCommand(Bootstrap bootstrap) {

        this.bootstrap = bootstrap;
    }


}
