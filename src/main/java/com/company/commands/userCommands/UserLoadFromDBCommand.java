package com.company.commands.userCommands;

import com.company.commands.AbstractCommand;
import com.company.service.UserService;

public class UserLoadFromDBCommand extends AbstractCommand {
    private UserService userService;

    @Override
    public String command() {
        return "user-load";
    }

    @Override
    public String description() {
        return "Load Users from file in Repository";
    }

    @Override
    public void execute() throws Exception {
        userService.load();
        System.out.println("База успешно загружена");
    }
    public boolean secureCommand() {
        return false;
    }

    public UserLoadFromDBCommand(UserService userService) {
        this.userService = userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
